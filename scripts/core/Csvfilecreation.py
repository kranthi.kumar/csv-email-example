import csv

import pandas as pd


class Csv_file:
    @staticmethod
    def csv_file(convert_data):
        list_row = []

        df = pd.DataFrame(convert_data, columns=["Minimun value", "maxmium Value ", "Average", "Maxmium value->>TimeStamp",
                                             "Minimum value->>TimeStamp"])
        df.to_csv('output.csv')
        with open('output.csv', 'r') as file:
            reader = csv.reader(file)
            print("Read", reader)
            for row in reader:
                list_row.append(",\t".join(row[1:]))
        return "\n".join(list_row)
# new_df = pd.read_csv('file1.csv')
# return new_df
