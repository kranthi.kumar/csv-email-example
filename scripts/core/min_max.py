from scripts.core.Timestamp import Timestamp


class Calculation:

    @staticmethod
    def calculation(data):
        required_data = []
        timestamp_list = []
        for (titles, columns_data) in data.iteritems():
            if titles != "Timestamp":
                list1 = list(columns_data.values)
                max_value = max(list1)
                min_value = min(list1)
                avg_value = (sum(list1) / len(list1))
                time_stand_data = Timestamp().time_stamp(min_value, max_value, timestamp_list, list1)
                min_timestamp = time_stand_data["minimumTimestamp"]
                max_timestamp = time_stand_data["maximumTimestamp"]
                required_data .append({"Title": str(titles),
                                         "Minimun value": min_value,
                                         "maxmium Value ": max_value,
                                         "Average": avg_value,
                                         "Maxmium value->>TimeStamp": max_timestamp,
                                         "Minimum value->>TimeStamp": min_timestamp})
            else:
                timestamp_list = list(columns_data.values)

        return required_data
