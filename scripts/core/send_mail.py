import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Send_Mail:
    @staticmethod
    def send_mail(message_recived):
        body = message_recived
        msg = MIMEMultipart()

        msg['Subject'] = 'Kranthi is sending a data in Csv file'
        msg['From'] = 'kranthikumar.mandapadu@gmail.com'
        msg['To'] = 'kranthi.kumar@knowledgelens.com'

        msg.attach(MIMEText(body, 'plain'))
        with open("input.csv", 'rb') as file:
            msg.attach(MIMEApplication(file.read(), Name="input.csv"))
        msg.attach(MIMEText(body, 'plain'))
        with open("output.csv", 'rb') as file:
            msg.attach(MIMEApplication(file.read(), Name="output.csv"))

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login('kranthikumar.mandapadu@gmail.com', 'GOR@Kranthi')
        server.send_message(msg)
        server.quit()
